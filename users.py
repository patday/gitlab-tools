import gitlab
import os


# private token or personal token authentication
gl = gitlab.Gitlab('http://gitlab.com', private_token=os.environ['GITLAB_TOKEN'])

group = gl.groups.get(4379867)
members = group.members.all(all=True)
for member in members:
    print(member.name)
print(len(members))
# for project in group.projects.list():
#     print(project)
