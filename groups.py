import datetime
import typer
from utils import gitlab_url_get

app = typer.Typer()


@app.command()
def list_inactive_billable_members(group_id: int = 4379867, inactivity_days: int = 60, export_path: str = None):
    group_members = list_billable_members(group_id)

    inactive_members = []
    days_inactivity_counter = 60

    today = datetime.datetime.now()
    for member in group_members:
        if member['last_activity_on']:
            member_date = datetime.datetime.strptime(member['last_activity_on'], '%Y-%m-%d')
            last_sign_in = today - member_date
            if last_sign_in.days > days_inactivity_counter:
                inactive_members.append(member)

    typer.secho(
        f'{len(inactive_members)} users have not logged in to GitLab in last {days_inactivity_counter} days:',
        fg=typer.colors.GREEN,
    )
    for member in inactive_members:
        typer.echo(f'{member["id"]} | {member["username"]} | {member["name"]}')

    if export_path:
        export_csv(inactive_members, export_path)


@app.command()
def list_billable_members(group_id: str = 4379867, export_path: str = None):
    group_members = gitlab_url_get(f'/groups/{group_id}/billable_members')
    typer.secho(
        f'{len(group_members)} billable members found in group {group_id}', fg=typer.colors.GREEN,
    )
    if export_path:
        export_csv(group_members, export_path)

    return group_members


@app.command()
def get_group_members(group_id: str, export_path: str = None):
    member_list = gitlab_url_get(f'/groups/{group_id}/members')
    typer.secho(
        f'{len(member_list)} members found in group {group_id}', fg=typer.colors.GREEN,
    )
    for member in member_list:
        typer.echo(f'{member["username"]} | {member["access_level"]}')
    if export_path:
        export_csv(member_list, export_path)
    return member_list


@app.command()
def list_group_tree(group_id: str = 4379867, export_path: str = None):
    group_list = gitlab_url_get(f'/groups/{group_id}/descendant_groups')
    typer.secho(
        f'{len(group_list)} descendant groups found in group {group_id}', fg=typer.colors.GREEN,
    )
    for group in group_list:
        typer.echo(group['full_path'])
    if export_path:
        export_csv(group_list, export_path)
    return group_list


if __name__ == "__main__":
    app()
