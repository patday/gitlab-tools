import csv
import os
import requests
import typer


def gitlab_url_get(path: str, base_url: str = 'https://gitlab.com/api/v4'):
    url = f'{base_url}{path}'

    curr_page = 1
    querystring = {"per_page": "100", "page": curr_page}

    payload = ""
    headers = {
        'PRIVATE-TOKEN': os.environ['GITLAB_TOKEN'],
    }

    typer.echo(f'Querying page: {querystring["page"]}')
    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)

    ret_obj = curr_obj = response.json()
    while len(curr_obj) > 0:
        querystring['page'] += 1
        typer.echo(f'Querying page: {querystring["page"]}')
        response = requests.request("GET", url, data=payload, headers=headers, params=querystring)
        curr_obj = response.json()
        ret_obj.extend(curr_obj)
    return ret_obj


def export_csv(gitlab_response: dict, export_path: str):
    typer.echo(f'Exporting CSV report to {export_path}')
    if len(gitlab_response) > 0:
        with open(export_path, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=gitlab_response[0].keys())
            writer.writeheader()
            for item in gitlab_response:
                writer.writerow(item)
